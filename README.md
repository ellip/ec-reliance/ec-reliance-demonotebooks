# EC-Reliance Demo Notebooks

## Content

This repos contains three Jupyter Notebooks that were developed for EC-Reliance project by Terradue. The three Notebooks consist of:

1) S1-SLC Data Discovery 
2) Service Execution 
3) Exploit Results

For the set-up, open a new terminal (New > Terminal) and execute all the shell commands below.

## Set-up
**Initialise the shell**
```
conda init bash
source /home/jovyan/.bashrc
```

**Create the `env_insar` environment, and then activate it**

```
mamba env create -f environment.yml
conda activate env_insar
```

**Install Jupyter Notebook kernel, with the **env_insar** environment just created**
```
ipython kernel install --name "env_insar" --user
```

Now, refresh the window and you will be able to select the **env_insar** kernel in the top-right of your Jupyter Notebook. 
